DOCKER_COMPOSE_RUN = docker-compose run --rm

PYTHON_ENV_SERVICE = python-stack
PYTHON_ENV_RUN = ${DOCKER_COMPOSE_RUN} ${PYTHON_ENV_SERVICE}

.PHONY: _test test

terminal:
	${PYTHON_ENV_RUN} python

bash:
	${PYTHON_ENV_RUN} bash

runp:
ifndef file
	make terminal
else
	${PYTHON_ENV_RUN} python ${file}
endif