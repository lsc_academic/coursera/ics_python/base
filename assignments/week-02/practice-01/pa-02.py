t = int(input("Por favor, entre com o número de segundos que deseja converter: "))

minutes = 60
hours = minutes * 60
days = hours * 24


d = t // days
h = ( t - d * days) // hours 
m = ( t - (d * days + h * hours)) // minutes
s = t - (d * days + h * hours + m * minutes)

print( d, "dias,", h, "horas", m, "minutos e", s, "segundos.")