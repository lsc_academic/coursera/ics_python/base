def primo(value):
  aux = 2

  while aux <= (value/2):
    if (value % aux) == 0:
      return False
      break
    else:
      aux += 1

  return True

def maior_primo(x):
  while x > 0:
    if primo(x):
      return x
    x -= 1
