def maximo(a, b):
  return max(a, b)

def test_maximo():
  assert maximo(1, 2) == 2
  assert maximo(1, -2) == 1
  assert maximo(5, 2) == 5
