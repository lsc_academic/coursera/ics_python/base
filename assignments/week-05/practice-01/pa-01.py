def fizzbuzz(a):
  if ((a % 3 == 0) or (a % 5 == 0)):
    if ((a % 3 == 0) and (a % 5 == 0)):
      return "FizzBuzz"
    elif (a % 3 == 0):
      return "Fizz"
    else:
      return "Buzz"
  else:
    return a